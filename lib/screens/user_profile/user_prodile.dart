import 'package:flutter/material.dart';
import 'package:telegram_profile/services/local_storage.dart';

class UserProfile extends StatefulWidget {
  final LocalStorageBox localStorageBox;

  const UserProfile({
    Key? key,
    required this.localStorageBox,
  }) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    final userModel = widget.localStorageBox.userBox.get(1);
    if (userModel != null) {
      return Center(
        child:
            Text('userId: ${userModel.id} \n userName: ${userModel.firstName}'),
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Text(
            'User poka jok...',
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 24.0,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 48.0),
            child: ElevatedButton(
              onPressed: () {
                _showModalBottomSheet(context);
              },
              child: const Text('Add new user'),
            ),
          )
        ],
      );
    }
  }

  void _showModalBottomSheet(context) {
    final formKey = GlobalKey<FormState>();
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(labelText: 'First Name'),
                    keyboardType: TextInputType.text,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(labelText: 'Last Name'),
                    keyboardType: TextInputType.text,
                  ),
                  TextFormField(
                    decoration: const InputDecoration(labelText: 'Phone Number'),
                    keyboardType: TextInputType.phone,
                  ),
                ],
              ),
            ),
          );
        });
  }
}
