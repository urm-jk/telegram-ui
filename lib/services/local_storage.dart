import 'package:objectbox/objectbox.dart';
import 'package:telegram_profile/models/user_model.dart';

class LocalStorageBox {
  /// The Store of this app.
  late final Store store;

  /// Two Boxes: one for Tasks, one for Tags.
  late final Box<UserModel> userBox;

  /// A stream of all tasks ordered by date.
  late final Stream<Query<UserModel>> tasksStream;

  LocalStorageBox(this.store) {
    userBox = Box<UserModel>(store);
  }

  UserModel? readUserModel(int userId) {
    return userBox.get(userId);
  }

  void writeOrUpdateUserModel(UserModel userModel) {
    userBox.put(userModel);
  }
}
