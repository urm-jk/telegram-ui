import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:get_it/get_it.dart';
import 'package:path/path.dart' as p;
import 'package:telegram_profile/screens/user_profile/user_prodile.dart';
import 'package:telegram_profile/services/local_storage.dart';

import 'objectbox.g.dart';

final sl = GetIt.instance;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final docsDir = await getApplicationDocumentsDirectory();
  final store = await openStore(directory: p.join(docsDir.path, 'ahsudevs'));
  sl.registerLazySingleton<LocalStorageBox>(() => LocalStorageBox(store));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        title: 'Telegram profile',
        debugShowCheckedModeBanner: false,
        home: HomePage());
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  final List<Widget> _widgetOptions = <Widget>[
    UserProfile(localStorageBox: sl()),
    const Center(
      child: Text('Calls Page',
          style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    ),
    const Center(
      child: Text('Chat Page',
          style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    ),
    const Center(
      child: Text('Settings Page',
          style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                color: Colors.black,
              ),
              label: '',
              activeIcon: Icon(
                Icons.person,
                color: Colors.green,
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.call,
                color: Colors.black,
              ),
              label: '',
              activeIcon: Icon(
                Icons.call,
                color: Colors.green,
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.chat_bubble_sharp,
                color: Colors.black,
              ),
              label: '',
              activeIcon: Icon(
                Icons.chat_bubble_sharp,
                color: Colors.green,
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.settings,
                color: Colors.black,
              ),
              label: '',
              activeIcon: Icon(
                Icons.settings,
                color: Colors.green,
              )),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
