import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:objectbox/objectbox.dart';

part 'user_model.g.dart';

@Entity()
@JsonSerializable()
class UserModel extends Equatable {
  int? id;
  String? firstName;
  String? lastName;
  String phoneNumber;

  UserModel({
    this.id,
    this.firstName,
    this.lastName,
    required this.phoneNumber,
  });

  UserModel copyWith({
    String? firstName,
    String? lastName,
    String? phoneNumber,
  }) {
    return UserModel(
      id: id,
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      phoneNumber: phoneNumber ?? this.phoneNumber,
    );
  }

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  @override
  String toString() => '${toJson()}';

  @override
  List<Object?> get props => [
        firstName,
        lastName,
        phoneNumber,
      ];
}
